"""
Code for transforming input data to suitable input and target data.
"""
import os
import tensorflow as tf
import tensorflow_transform as tft


def transformed_name(key: str) -> str:
    return key + "_xf"


def preprocessing_fn(inputs: tf.Tensor) -> tf.Tensor:
    None
