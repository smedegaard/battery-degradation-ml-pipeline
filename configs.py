"""TFX obj-det template configurations.

This file defines environments for a TFX obj-det pipeline.
"""

import os  # pylint: disable=unused-import

# Pipeline name will be used to identify this pipeline.
PIPELINE_NAME = "obj-det-ml-pipeline"

# GCP related configs.

# Following code will retrieve your GCP project. You can choose which project
# to use by setting GOOGLE_CLOUD_PROJECT environment variable.
try:
    import google.auth  # pylint: disable=g-import-not-at-top  # pytype: disable=import-error

    try:
        _, GOOGLE_CLOUD_PROJECT = google.auth.default()
    except google.auth.exceptions.DefaultCredentialsError:
        GOOGLE_CLOUD_PROJECT = ""
except ImportError:
    GOOGLE_CLOUD_PROJECT = ""

# Specify your GCS bucket name here. You have to use GCS to store output files
# when running a pipeline with Kubeflow Pipeline on GCP or when running a job
# using Dataflow. Default is '<gcp_project_name>-kubeflowpipelines-default'.
# This bucket is created automatically when you deploy KFP from marketplace.
GCS_BUCKET_NAME = GOOGLE_CLOUD_PROJECT + "-kubeflowpipelines-default"


PREPROCESSING_FN = "pipeline.image_transform.preprocessing_fn"
RUN_FN = "pipeline.trainer.run_fn"

TRAIN_NUM_STEPS = 10
EVAL_NUM_STEPS = 1

# Change this value according to your use cases.
EVAL_ACCURACY_THRESHOLD = 0.6
